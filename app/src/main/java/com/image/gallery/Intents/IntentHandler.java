package com.image.gallery.Intents;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.widget.Toast;

import com.image.gallery.R;

/**
 * Created by Xpieon on 20/05/2016.
 */
public class IntentHandler {

    /**
     *
     * @param context Context of the current activity
     * @param uri Uri of the file to be sent via intent
     */

    public static void sendAsEmailIntent(Context context, Uri uri){

        Resources resources = context.getResources();

        try {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("application/image");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT,"");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
            context.startActivity(Intent.createChooser(emailIntent, resources.getString(R.string.send_mail)));
        } catch (Exception e) {
            Toast.makeText(context,resources.getString(R.string.no_intent_to_handle) , Toast.LENGTH_SHORT).show();
        }

    }

    /**
     *
     * @param context Context of the current activity
     * @param uri Uri of the file to be sent via intent
     */
    public static void openImageIntent(Context context,Uri uri){

        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "image/*");
            context.startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(context,context.getResources().getString(R.string.no_intent_to_handle) , Toast.LENGTH_SHORT).show();
        }

    }

}
