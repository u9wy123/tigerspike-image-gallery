package com.image.gallery.Utils;

import android.content.Context;
import android.os.Environment;

import com.image.gallery.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;

/**
 * Created by Xpieon on 19/05/2016.
 */
public class FileUtil {

    /**
     * Removes characters that do not fit file names and transforms the url path into a file path located in the applications data cache directory.
     * @param context The current activity context
     * @param url The url path to be formatted
     * @returns Returns a File object of the converted url
     */

    public static File urlToFilePath(Context context,URL url){

        String urlString = url.getPath();
        urlString = urlString.replaceAll("[/\\.:'*?|<>£%&$!@+`_= ]", "-").trim();

        return new File(context.getCacheDir() + "/" + urlString);
    }

    public static File urlToFilePath(Context context, String url){

        try {
            return urlToFilePath(context,new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method retrieves the external storage application directory. If the directory does not currently exist it is created then returned.
     * @return File Object of Application directory.
     */
    public static File getApplicationDirectory(Context context) {

        File file = new File(Environment.getExternalStorageDirectory() + "/" + context.getResources().getString(R.string.app_name));

        if (!file.exists())
            file.mkdir();

        return file;
    }

    /**
     * This method retrieves the external storage application temp directory. If the directory does not currently exist it is created then returned.
     * @return File Object of Temp directory.
     */
    public static File getApplicationTempDirectory(Context context) {

        File file = new File(getApplicationDirectory(context).getPath() + "/.temp");

        if (!file.exists())
            file.mkdir();

        return file;
    }

    /**
     * Copies a file from the source to the destination
     * @param sourceFile File Object of the source file
     * @param destinationFile File Object of the destination file
     * @return The destination file
     */

    public static File copyFile(File sourceFile, File destinationFile){

        FileInputStream inStream = null;
        OutputStream outStream = null;
        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try {
            inStream = new FileInputStream(sourceFile);

            outStream = new FileOutputStream(destinationFile);
            inChannel = inStream.getChannel();
            outChannel = ((FileOutputStream) outStream).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inStream.read(buffer)) != -1)
                outStream.write(buffer, 0, bytesRead);

        } catch (IOException e) {
            e.printStackTrace();
        }


        if (inStream != null)
            try {
                inStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if (outStream != null)
            try {
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if (inChannel != null)
            try {
                inChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if (outChannel != null)
            try {
                outChannel.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        return destinationFile;
    }

}
