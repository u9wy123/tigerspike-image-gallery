package com.image.gallery.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Xpieon on 20/05/2016.
 */
public class DateUtil {

    /**
     * Converts the formatted string date into a long
     * @param formattedDate formated date String
     * @return
     */

    public static long formattedDateToLong(String formattedDate){

        if(formattedDate != null){
            formattedDate = formattedDate.substring(0,formattedDate.indexOf("T")-1);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = simpleDateFormat.parse(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null)
                return date.getTime();
        }

        return 0L;
    }

    /**
     * Converts date in milliseconds to String
     * @param milliseconds date in milliseconds
     * @return Date formatted string "yyy-MM-dd
     */

    public static String formatDate(long milliseconds){

        Date date = new Date(milliseconds);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return simpleDateFormat.format(date);
    }

}
