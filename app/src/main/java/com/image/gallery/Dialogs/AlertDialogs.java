package com.image.gallery.Dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.image.gallery.Adapters.Images_Grid_Adapter;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrFeed;
import com.image.gallery.Intents.IntentHandler;
import com.image.gallery.Utils.DateUtil;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrImage;
import com.image.gallery.R;
import com.image.gallery.Utils.FileUtil;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Xpieon on 20/05/2016.
 */
public class AlertDialogs {

    /**
     * Displays a metadata dialog for the selected Image
     * @param context Current activity context
     * @param flickrImage The selected FlickrImage Object
     */

    public static void displayMetaDataDialog(Context context,FlickrImage flickrImage){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View inflatedView = inflater.inflate(R.layout.custom_alert_dialog_metadata, null);

        Resources resources = context.getResources();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(inflatedView);
        builder.setTitle(resources.getString(R.string.alert_dialog_title_meta_data));

        TextView textViewTitle = (TextView) inflatedView.findViewById(R.id.textViewTitle_metadata);
        TextView textViewLink = (TextView) inflatedView.findViewById(R.id.textViewLink_metadata);
        TextView textViewMedia = (TextView) inflatedView.findViewById(R.id.textViewMedia_metadata);
        TextView textViewDateTaken = (TextView) inflatedView.findViewById(R.id.textViewDateTaken_metadata);
        TextView textViewPublished = (TextView) inflatedView.findViewById(R.id.textViewPublished_metadata);
        TextView textViewAuthor = (TextView) inflatedView.findViewById(R.id.textViewAuthor_metadata);
        TextView textViewAuthroID = (TextView) inflatedView.findViewById(R.id.textViewAuthorID_metadata);
        TextView textViewTags = (TextView) inflatedView.findViewById(R.id.textViewTags_metadata);
        TextView textViewDescription = (TextView) inflatedView.findViewById(R.id.textViewDescription_metadata);

        textViewTitle.setText(flickrImage.getTitle());
        textViewLink.setText(flickrImage.getLink());
        textViewMedia.setText(flickrImage.getMedia());
        textViewDateTaken.setText(DateUtil.formatDate(flickrImage.getDateTaken()));
        textViewPublished.setText(DateUtil.formatDate(flickrImage.getPublished()));
        textViewAuthor.setText(flickrImage.getAuthor());
        textViewAuthroID.setText(flickrImage.getAuthorID());
        textViewTags.setText(flickrImage.getTags());
        textViewDescription.setText(flickrImage.getDescription());

        builder.setNegativeButton(resources.getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AppCompatDialog appCompatDialog =  builder.create();
        appCompatDialog.show();

    }

    /**
     * Displays a sort dialog
     * @param context Current activity context
     * @param flickrFeed The current FlickrFeed Object
     * @param adapter The current adapter being used to display data
     */

    public static void displaySortAlertDialog(Context context, final FlickrFeed flickrFeed, final Images_Grid_Adapter adapter){

        String [] listItems = context.getResources().getStringArray(R.array.sort_dialog_array);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.sort_order));
        alertDialogBuilder.setItems(listItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0:
                        Collections.sort(flickrFeed.getItems(), new Comparator<FlickrImage>() {
                            @Override
                            public int compare(FlickrImage s1, FlickrImage s2) {
                                return String.valueOf(s1.getDateTaken()).compareTo(String.valueOf(s2.getDateTaken()));
                            }
                        });
                        break;
                    case 1:
                        Collections.sort(flickrFeed.getItems(), new Comparator<FlickrImage>() {
                            @Override
                            public int compare(FlickrImage s1, FlickrImage s2) {
                                return String.valueOf(s2.getDateTaken()).compareTo(String.valueOf(s1.getDateTaken()));
                            }
                        });
                        break;
                }
                dialog.dismiss();
                adapter.notifyDataSetChanged();
            }
        });

        AppCompatDialog appCompatDialog =  alertDialogBuilder.create();
        appCompatDialog.show();

    }

    /**
     * Displays export options dialog
     * @param context Context of the current activity
     * @param imageFile File object of the imageFile to be actioned
     */

    public static void displayExportOptionsDialog(final Context context, final File imageFile){

        String [] listItems = context.getResources().getStringArray(R.array.export_options_array);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.export_options));
        alertDialogBuilder.setItems(listItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, final int which) {

                MediaScannerConnection.scanFile(context, new String[]{imageFile.getPath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {

                                switch (which) {
                                    case 0:
                                        final boolean success = imageFile.renameTo(new File(FileUtil.getApplicationDirectory(context).getPath() + "/" + imageFile.getName()));

                                        final Resources resources = context.getResources();

                                        new Handler(context.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(success)
                                                    Toast.makeText(context,resources.getString(R.string.image_exported) + " " + FileUtil.getApplicationDirectory(context).getPath() + "/" + imageFile.getName(),Toast.LENGTH_SHORT).show();
                                                else
                                                    Toast.makeText(context,resources.getString(R.string.image_export_failed),Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        break;
                                    case 1:
                                        IntentHandler.openImageIntent(context,uri);
                                        break;
                                    case 2:
                                        IntentHandler.sendAsEmailIntent(context,uri);
                                        break;
                                }

                            }
                        });
                dialog.dismiss();
            }
        });

        AppCompatDialog appCompatDialog =  alertDialogBuilder.create();
        appCompatDialog.show();

    }

}
