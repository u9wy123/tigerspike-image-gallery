package com.image.gallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Xpieon on 20/05/2016.
 */
public class Permissions {

    public static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    /**
     * Will prompt the user to grant the specified permission
     * @param context Context of the current activity
     * @param message A message to display if needed
     * @param permission Permission to be requested
     * @param requestCode A request code identifier
     */

    public static void requestPermission(final Context context,String message, final String permission, final int requestCode) {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission)) {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setCancelable(false);
            alertDialog.setTitle(context.getResources().getString(R.string.title_warning));
            alertDialog.setMessage(message);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(R.string.OK_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, requestCode);
                }
            });
            alertDialog.show();

        } else {
            ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, requestCode);

        }
    }

    /**
     * Checks if the specified permission has been granted
     * @param context Context of the current activity
     * @param permission Permission to check
     * @return True if permissions are granted otherwise false is returned
     */

    public static boolean checkPermission(Context context, String permission){

        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

}
