package com.image.gallery.FlickrWrapper.ResponseObjects;

import java.util.Comparator;

public class FlickrImage implements Comparator {

	private String title,link,media,description,author,authorID,tags;

	private long dateTaken,published;

	public FlickrImage(String title, String link, String media, long dateTaken, String description, long published, String author, String authorID, String tags){
		this.title = title;
		this.link = link;
		this.media = media;
		this.dateTaken = dateTaken;
		this.description = description;
		this.published = published;
		this.author = author;
		this.authorID = authorID;
		this.tags = tags;
	}

	/**
	 * @return The Image Title
	 */

	public String getTitle() {
		return title;
	}

	/**
	 * @return The URL link of the image on the Flickr website
	 */

	public String getLink() {
		return link;
	}

	/**
	 * @return The URL to the image file
	 */

	public String getMedia() {
		return media;
	}

	/**
	 * @return The Description of the image
	 */

	public String getDescription() {
		return description;
	}

	/**
	 * @return A long of the date the image was published in milliseconds
	 */

	public long getPublished() {
		return published;
	}

	/**
	 * @return The Author of the image
	 */

	public String getAuthor() {
		return author;
	}

	/**
	 * @return The ID of the author
	 */

	public String getAuthorID() {
		return authorID;
	}

	/**
	 * @return The tags associated with the image
	 */

	public String getTags() {
		return tags;
	}

	/**
	 * @return A long of the date the image was taken in milliseconds
	 */

	public long getDateTaken() {
		return dateTaken;
	}

	@Override
	public int compare(Object lhs, Object rhs) {
		return 0;
	}
}
