package com.image.gallery.FlickrWrapper.ResponseObjects;

import java.util.ArrayList;

public class FlickrFeed {

	private String title,link,description,generator;
	private long modified;

	private ArrayList<FlickrImage> items;
	
	public FlickrFeed(String title, String link, String description, long modified, String generator,ArrayList<FlickrImage> items){
		this.title = title;
		this.link = link;
		this.description = description;
		this.modified = modified;
		this.generator = generator;
		this.items = items;
	}

	/**
	 * @return The Flickr Feed Title
	 */

	public String getTitle(){
		return title;
	}

	/**
	 * @return The Flickr Feed Link
	 */

	public String getLink(){
		return link;
	}

	/**
	 * @return The Flickr Feed Description
	 */

	public String getDescription(){
		return description;
	}

	/**
	 * @return The Flickr Feed modified date in milliseconds
	 */

	public long getModified(){
		return modified;
	}

	/**
	 * @return The Flickr Feed source
	 */

	public String getGenerator(){
		return generator;
	}

	/**
	 * @return An ArrayList of images contained within the Flickr Feed
	 */

	public ArrayList<FlickrImage> getItems() {
		return items;
	}

	/**
	 * Represents the object in a string format
	 * @return A string
	 */

	public String toString(){

		String string = "title: " + title + "\n" +
				"link: " + link + "\n" +
				"description: " + description + "\n" +
				"generator: " + generator + "\n" +
				"modified: " + String.valueOf(modified) + "\n\n" +
				"items: [";

		for (int i = 0 ; i < items.size(); i++)
			string = string + items.get(i).getMedia() + ", \n";

		string = string + "]";

		return string;
	}

}
