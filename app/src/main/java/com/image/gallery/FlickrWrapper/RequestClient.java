package com.image.gallery.FlickrWrapper;

import com.image.gallery.Utils.DateUtil;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrFeed;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrImage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestClient {

	private final static String ENDPOINT = "https://api.flickr.com/services/feeds/photos_public.gne";
	
	private final static String FORMAT = "?format=json";
	
	private final static String SEARCH_TAG = "&tags=";
	
	public static boolean LOADING = false;

	/**
	 * Loads the flickr image feed
	 *
	 * @return A FlickrFeed object
	 */

	public static FlickrFeed getFlickrFeed(){
		
		return sendGetRequest(null);
	}

	/**
	 * Loads the flickr image feed results are based on the search tag
	 * @param searchTag A search tag to filter the results
	 * @return A FlickrFeed object
	 */

	public static FlickrFeed getFlickrFeed(String searchTag){
		
		return sendGetRequest(searchTag);
	}


	/**
	 * Sends the HTTP request and converts the response into a FlickrFeed object
	 * @param searchTag search string... can be null
	 * @return A FlickrFeed object
	 */

	private static FlickrFeed sendGetRequest(String searchTag){

		LOADING = true;

		URL url;
		
		if(searchTag == null)
			url = formatURLString(ENDPOINT + FORMAT);
		else
			url = formatURLString(ENDPOINT + FORMAT + SEARCH_TAG + searchTag);

		if(url == null) {
			LOADING = false;
			return null;
		}

		HttpURLConnection connection;
		FlickrFeed flickrFeed = null;

		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(10000);
			int responseCode = connection.getResponseCode();
			
			
			if (responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				
				String inputLine;
				StringBuilder response = new StringBuilder();

				while ((inputLine = in.readLine()) != null) 
					response.append(inputLine);
				
				in.close();
				
				String responseString = response.toString();
				flickrFeed = readResponseString(responseString);
				
			}else {
				LOADING = false;
				return null;
			}
			
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		} 

		LOADING = false;
		
		return flickrFeed;
	}

	/**
	 * Converts the JSON formatted string into a FlickrFeed Object
	 * @param responseString The response from the http request
	 * @return The FlickrFeed Object
	 * @throws JSONException
	 */

	private static FlickrFeed readResponseString(String responseString) throws JSONException{
		
		responseString = responseString.substring(responseString.indexOf("(")+1,responseString.length()-1);
		JSONObject jsonObject = new JSONObject(responseString);
		
		String title = jsonObject.getString("title");
		String link = jsonObject.getString("link");
		String description = jsonObject.getString("description");
		String modified = jsonObject.getString("modified");
		String generator = jsonObject.getString("generator");
		
		JSONArray jsonArray = jsonObject.getJSONArray("items");

		ArrayList<FlickrImage> flickrImages = new ArrayList<>();
		
		for (int i = 0; i < jsonArray.length(); i++){
			
			JSONObject item = jsonArray.getJSONObject(i);
			flickrImages.add(new FlickrImage(item.getString("title"),
					item.getString("link"),
					item.getJSONObject("media").getString("m"),
					DateUtil.formattedDateToLong(item.getString("date_taken")),
					item.getString("description"),
					DateUtil.formattedDateToLong(item.getString("published")),
					item.getString("author"),
					item.getString("author_id"),
					item.getString("tags")));
		}
		
		return new FlickrFeed(title,link,description, DateUtil.formattedDateToLong(modified),generator,flickrImages);
	}

	/**
	 * Properly formats the url string
	 * @param urlString The url string to be formatted
	 * @return A URL Object
	 */

	private static URL formatURLString(String urlString){

        URL url = null;
		try {
			url = new URL(urlString);
	        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
	        url = uri.toURL();
		} catch (MalformedURLException | URISyntaxException e) {
			e.printStackTrace();
			try {
				url = new URL(urlString);
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			}
		}
		
		return url;
	}
	
}
