package com.image.gallery.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.image.gallery.Adapters.Images_Grid_Adapter;
import com.image.gallery.Dialogs.AlertDialogs;
import com.image.gallery.Dialogs.ProgressDialogFactory;
import com.image.gallery.FlickrWrapper.RequestClient;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrFeed;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrImage;
import com.image.gallery.Permissions;
import com.image.gallery.R;
import com.image.gallery.Utils.FileUtil;

import java.io.File;

/**
 * Created by Xpieon on 19/05/2016.
 */
public class Main_Activity extends AppCompatActivity implements View.OnClickListener, Images_Grid_Adapter.OnItemClickListener, Images_Grid_Adapter.OnItemLongClickListener {


    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButtonRefresh;
    private ProgressDialog progressDialog;
    private SearchView searchView;

    private Images_Grid_Adapter imagesGridAdapter;
    private FlickrFeed flickrFeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_images);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        floatingActionButtonRefresh = (FloatingActionButton) findViewById(R.id.button_floating_action_button_refresh);
        floatingActionButtonRefresh.setOnClickListener(this);
        progressDialog = ProgressDialogFactory.createProgressDialog(this, null, getResources().getString(R.string.please_wait), false);

        loadFlickrFeed(true, null);

    }

    /**
     * Loads the flickr feed and displays the images in the recycler view
     *
     * @param setUpAdapter Whether the adapter should be initialised
     * @param searchText   Text to filter the search by
     */

    private void loadFlickrFeed(final boolean setUpAdapter, final String searchText) {

        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (searchText == null)
                    flickrFeed = RequestClient.getFlickrFeed();
                else
                    flickrFeed = RequestClient.getFlickrFeed(searchText);

                if (flickrFeed == null) {
                    progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Main_Activity.this, getResources().getString(R.string.error_loading), Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        if (setUpAdapter || imagesGridAdapter == null) {
                            imagesGridAdapter = new Images_Grid_Adapter(Main_Activity.this, flickrFeed);
                            imagesGridAdapter.setOnItemClickListener(Main_Activity.this);
                            imagesGridAdapter.setOnItemLongClickListener(Main_Activity.this);
                            recyclerView.setAdapter(imagesGridAdapter);
                        } else
                            imagesGridAdapter.setFlickrFeed(flickrFeed);

                        if (progressDialog.isShowing())
                            progressDialog.dismiss();

                    }
                });

            }
        }).start();

    }

    /**
     * Prepares the image files and shows the export alert dialog
     *
     * @param position The position of the selected image
     */

    private void exportDialog(int position) {

        final FlickrImage flickrImage = flickrFeed.getItems().get(position);

        new Thread(new Runnable() {
            @Override
            public void run() {



                final File imageFile = FileUtil.copyFile(FileUtil.urlToFilePath(Main_Activity.this, flickrImage.getMedia()),
                        new File(FileUtil.getApplicationTempDirectory(Main_Activity.this).getPath() + "/" + flickrImage.getTitle() + ".jpg"));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialogs.displayExportOptionsDialog(Main_Activity.this, imageFile);
                    }
                });

            }
        }).start();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main_activity, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setQueryHint(getString(R.string.menu_search_hint));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                loadFlickrFeed(false, s);
                searchView.onActionViewCollapsed();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_sort:
                AlertDialogs.displaySortAlertDialog(this, flickrFeed, imagesGridAdapter);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_floating_action_button_refresh:
                searchView.onActionViewCollapsed();
                if (!RequestClient.LOADING) {
                    loadFlickrFeed(false, null);
                } else
                    Toast.makeText(this, getString(R.string.loading_alert), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        AlertDialogs.displayMetaDataDialog(this, flickrFeed.getItems().get(position));
    }

    @Override
    public void onItemLongClick(View view, int position) {

        if (!Permissions.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            Permissions.requestPermission(this,
                    this.getResources().getString(R.string.alert_message_write_permission),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Permissions.REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION);
        else
            exportDialog(position);

    }

    @Override
    protected void onDestroy() {

        final File tempPath = FileUtil.getApplicationTempDirectory(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (File image : tempPath.listFiles())
                    image.delete();
            }
        }).start();

        imagesGridAdapter.stopLoader();
        super.onDestroy();
    }
}
