package com.image.gallery.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.image.gallery.Activities.Main_Activity;
import com.image.gallery.FlickrWrapper.ResponseObjects.FlickrFeed;
import com.image.gallery.ImageHandling.ImageLoader;
import com.image.gallery.R;

/**
 * Created by Xpieon on 19/05/2016.
 */
public class Images_Grid_Adapter extends RecyclerView.Adapter<Images_Grid_Adapter.ViewHolder> {

    private Context context;
    private FlickrFeed flickrFeed;
    private ImageLoader imageLoader;
    private Resources resources;
    private OnItemClickListener onItemClickListener;
    private OnItemLongClickListener onItemLongClickListener;

    /**
     * Initialises the grid adapter
     * @param context Current activity context
     * @param flickrFeed The FlickrFeed object
     */

    public Images_Grid_Adapter(Context context,FlickrFeed flickrFeed){
        this.context = context;
        this.flickrFeed = flickrFeed;
        imageLoader = new ImageLoader(context);
        resources = context.getResources();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        ImageView imageView;

        public ViewHolder(View view){
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);

            Display display = ((Main_Activity)context).getWindowManager().getDefaultDisplay();
            DisplayMetrics outMetrics = new DisplayMetrics();
            display.getMetrics(outMetrics);

            RelativeLayout.LayoutParams imageParams = new RelativeLayout.LayoutParams(outMetrics.widthPixels/2,outMetrics.widthPixels/2);
            imageView.setLayoutParams(imageParams);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onItemClickListener != null)
                onItemClickListener.onItemClick(view,getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            if(onItemLongClickListener != null)
                onItemLongClickListener.onItemLongClick(view,getAdapterPosition());
            return true;
        }
    }

    @Override
    public Images_Grid_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_image_grid, parent, false);

        return new ViewHolder(view);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        Drawable drawable;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            drawable = resources.getDrawable(R.drawable.ic_file_image_grey600_48dp,context.getTheme());
        else
            drawable = resources.getDrawable(R.drawable.ic_file_image_grey600_48dp);

        imageLoader.loadImage(flickrFeed.getItems().get(position).getMedia(),viewHolder.imageView,drawable);

    }

    @Override
    public int getItemCount() {
        return flickrFeed.getItems().size();
    }

    /**
     * Sets a new FlickrFeed object and notifies the adapter to update
     * @param flickrFeed The new FlickrFeed object
     */

    public void setFlickrFeed(FlickrFeed flickrFeed){
        this.flickrFeed = flickrFeed;
        notifyDataSetChanged();
    }

    /**
     * Stops the imageLoader from loading images.
     */

    public void stopLoader(){
        imageLoader.stop();
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public interface OnItemLongClickListener{
        void onItemLongClick(View view, int position);
    }

    /**
     * Registers a call back to be called when an item is clicked
     * @param ItemClickListener The call back that will run
     */
    public void setOnItemClickListener(OnItemClickListener ItemClickListener) {
        onItemClickListener = ItemClickListener;
    }

    /**
     * Registers a call back to be called when an item is long clicked
     * @param ItemLongClickListener The call back that will run
     */
    public void setOnItemLongClickListener(OnItemLongClickListener ItemLongClickListener) {
        onItemLongClickListener = ItemLongClickListener;
    }

}
