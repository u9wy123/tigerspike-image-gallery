package com.image.gallery.ImageHandling;

import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Xpieon on 23/05/2016.
 */

/**
 * Keeps track of which thread is loading content for a certain ImageView to prevent recycled
 * ImageViews from displaying content loaded on different threads for ImageViews which are no longer visible
 */
public class Requests {

    private ArrayList<Integer> threadIDs = new ArrayList<>();
    private ArrayList<ImageView> targets = new ArrayList<>();

    /**
     * Adds a request
     * @param threadID The thread ID
     * @param target The target ImageView
     */

    public void addRequest(int threadID, ImageView target){
        threadIDs.add(threadID);
        targets.add(target);
    }

    /**
     * Removes a request when a matching ImageView is found
     * @param target The target ImageView
     */

    public void removeRequest(ImageView target){
        for (int i = 0; i < targets.size(); i++)
            if(targets.get(i) == target){
                targets.remove(i);
                threadIDs.remove(i);
            }
    }

    /**
     * Removes a request when a matching ImageView and thread ID is found
     * @param target The target ImageView
     * @param threadID The thread ID
     */

    public void removeRequest(ImageView target, int threadID){
        for (int i = 0; i < threadIDs.size(); i++)
            if(targets.get(i) == target && threadIDs.get(i) == threadID){
                targets.remove(i);
                threadIDs.remove(i);
            }
    }

    /**
     * Checks if a request already exists based on the thread ID
     * @param threadID The thread ID
     * @return A boolean indicating if this thread is already handling a request
     */

    public boolean requestExists(int threadID){
        for (int i = 0; i < threadIDs.size(); i++)
            if(threadIDs.get(i) == threadID)
                return true;

        return false;
    }

    /**
     * Checks if a request already exists based on the ImageView
     * @param target The ImageView
     * @return A boolean indicating if a thread is already handling a request for this ImageView
     */

    public boolean requestExists(ImageView target){
        for (int i = 0; i < targets.size(); i++)
            if(targets.get(i) == target)
                return true;

        return false;
    }

}
