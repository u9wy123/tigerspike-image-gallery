package com.image.gallery.ImageHandling;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.widget.ImageView;

import com.image.gallery.Utils.FileUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Xpieon on 19/05/2016.
 */
public class ImageLoader {

    private Context context;

    private ExecutorService executorService = Executors.newFixedThreadPool(5);

    private ArrayList<Integer> IDs = new ArrayList<>();

    private Requests requests = new Requests();

    public ImageLoader(Context context){
        this.context = context;
    }

    /**
     * Loads the image from the given source
     *
     * @param urlSource The String URL of the image that should be downloaded
     * @param Target The target ImageView where the image should be displayed
     * @param placeHolder A temporary image to be displayed whilst the image is downloading, this can be null
     */

    public void loadImage(final String urlSource, final ImageView Target, final Drawable placeHolder){

        setImage(context, Target, ImageView.ScaleType.CENTER_INSIDE, placeHolder);

        if(requests.requestExists(Target))
            requests.removeRequest(Target);

        int threadID = 0;

        if (!IDs.isEmpty())
            threadID = IDs.get(IDs.size()-1)+1;

        IDs.add(threadID);
        requests.addRequest(threadID, Target);

        final int finalThreadID = threadID;
        executorService.submit(new Runnable() {
            @Override
            public void run() {

                URL Source = null;
                try {
                    Source = new URL(urlSource);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                if (Source == null) {
                    return;
                }

                File imageFile = FileUtil.urlToFilePath(context, Source);

                if (imageFile.exists()) {
                    setImage(context, Target, ImageView.ScaleType.CENTER_CROP, imageFile);
                    return;
                }

                boolean success = downloadImage(Source, imageFile);

                if (success && requests.requestExists(finalThreadID))
                    setImage(context, Target, ImageView.ScaleType.CENTER_CROP, imageFile);
                else
                    setImage(context, Target, ImageView.ScaleType.CENTER_INSIDE, placeHolder);

                IDs.remove(finalThreadID);
                requests.removeRequest(Target,finalThreadID);
            }
        });
    }

    /**
     * Sets the target with the provided image
     * @param context Current activity context
     * @param Target The Target ImageView
     * @param scaleType The scale type to be set for the ImageView
     * @param imageFile The file to be displayed in the ImageView
     */

    private void setImage(Context context, final ImageView Target, final ImageView.ScaleType scaleType, final File imageFile){

        setImage(context,Target,scaleType,Drawable.createFromPath(imageFile.getPath()));
    }

    /**
     * Sets the target with the provided image
     * @param context Current activity context
     * @param Target The Target ImageView
     * @param scaleType The scale type to be set for the ImageView
     * @param drawable The drawable to be displayed in the ImageView
     */

    private void setImage(Context context, final ImageView Target, final ImageView.ScaleType scaleType, final Drawable drawable){

        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                Target.setScaleType(scaleType);
                Target.setImageDrawable(drawable);

            }
        });
    }

    /**
     * Downloads the image from the URL source
     * @param url The URL path where the image is located
     * @param destinationFile  The File which the image should be downloaded into
     * @return A boolean indicating success or failure
     */

    public static boolean downloadImage(URL url, File destinationFile){

        boolean success = false;

        try {
            success = destinationFile.createNewFile();
            InputStream inputStream = url.openStream();
            OutputStream outputStream = new FileOutputStream(destinationFile);

            byte[] b = new byte[2048];
            int length;

            while ((length = inputStream.read(b)) != -1)
                outputStream.write(b, 0, length);


            inputStream.close();
            outputStream.close();
            success = true;

        } catch (IOException e) {
            e.printStackTrace();
            destinationFile.delete();
        }

        return success;
    }

    /**
     * Shuts down executor service
     */

    public void stop(){
        executorService.shutdownNow();
    }

}
